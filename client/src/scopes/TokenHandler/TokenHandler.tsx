import React from "react";
import { Redirect } from "react-router-dom";

import { useAppContext } from "../../contexts/AppContext";

export default function TokenHandler() {
  const { token } = useAppContext();

  if (token) {
    return <Redirect to="/trade" />;
  }

  return <Redirect to="/login" />;
}
