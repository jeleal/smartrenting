import React, { useEffect, useState } from "react";
import {
  Container,
  Divider,
  Form,
  Grid,
  Input,
  Label,
} from "semantic-ui-react";
import { Order } from "../../constants/types";
import TableOfOrders from "./TableOfOrders";

import "./Trade.css";
import { useAppContext } from "../../contexts/AppContext";
import Post from "../../utils/Post";
import { Statistics } from "./Statistics";

export default function Trade({ userId }: { userId: number }) {
  const [orders, setOrders] = useState<Order[]>([]);
  const [priceOrder, setPriceOrder] = useState<number>();
  const [error, setError] = useState<string>("");
  const { token, setToken } = useAppContext();
  const [refetch, setRefetch] = useState<boolean>(false);

  useEffect(() => {
    Post({ uri: "/order", data: null, token })
      .then(({ data }) => {
        setOrders(data);
      })
      .catch((err) => {
        if (err.response.status === 401 && token) {
          localStorage.removeItem("token");
          setToken(null);
        }
      });
  }, [token, refetch]);

  const createOrder = () => {
    if (priceOrder && priceOrder > 0) {
      Post({ uri: "/order/create", data: { price: priceOrder }, token })
        .then(() => {
          setError("");
          setPriceOrder(null);
          setRefetch((val) => !val);
        })
        .catch((err) => {
          if (err.response.status === 401 && token) {
            localStorage.removeItem("token");
            setToken(null);
          } else {
            setRefetch((val) => !val);
          }
        });
    } else {
      setError("The price must be greater than 0 and a valid number");
    }
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { target } = e;

    if (target.value !== "") {
      setPriceOrder(parseInt(target.value));
    } else {
      setPriceOrder(null);
    }
  };

  return (
    <Grid
      style={{
        height: "100vh",
        marginTop: "50px",
        marginLeft: "5px",
        marginRight: "5px",
        marginBottom: "0px",
      }}
    >
      <Grid.Column>
        <Container fluid textAlign="left">
          <Grid.Row>
            <Form onSubmit={createOrder}>
              <Input
                error={error !== ""}
                type="number"
                className="black-input "
                value={priceOrder || ""}
                placeholder="Price (10$)"
                onChange={handleChange}
              />
              <button className="ui button side-margin-10">Create order</button>
              {error && (
                <Label pointing="left" color="red">
                  {error}
                </Label>
              )}
            </Form>
          </Grid.Row>
          <Grid.Row>
            <h2 style={{ margin: "10px 0" }}>Orders:</h2>
            <TableOfOrders
              orders={orders}
              userId={userId}
              refetch={setRefetch}
            />
          </Grid.Row>
          <Divider />
          <Grid.Row>
            <h2 style={{ margin: "10px 0" }}>Statistics:</h2>
            <Statistics orders={orders} />
          </Grid.Row>
        </Container>
      </Grid.Column>
    </Grid>
  );
}
