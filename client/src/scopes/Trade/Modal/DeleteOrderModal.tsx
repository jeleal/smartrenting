import React, { SetStateAction, Dispatch, useState, useEffect } from "react";
import { Header, Icon, Modal } from "semantic-ui-react";
import { Order } from "../../../constants/types";
import Post from "../../../utils/Post";
import moment from "moment";
import { useAppContext } from "../../../contexts/AppContext";

export const DeleteOrderModal = ({
  open,
  order,
  setOpen,
  refetch,
}: {
  open: boolean;
  order: Order;
  setOpen: Dispatch<SetStateAction<boolean>>;
  refetch: Dispatch<SetStateAction<boolean>>;
}) => {
  const { token, setToken } = useAppContext();
  const deleteOrder = () => {
    Post({ uri: "/order/delete", data: { id: order.id }, token })
      .then(({ data }) => {
        if (data === "success") {
          refetch((val) => !val);
          setOpen(false);
        }
      })
      .catch((err) => {
        if (err.response.status === 401 && token) {
          localStorage.removeItem("token");
          setToken(null);
        } else {
          refetch((val) => !val);
          setOpen(false);
        }
      });
  };

  return (
    <Modal
      basic
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      size="small"
      style={{ textAlign: "center" }}
    >
      <Header icon>
        <Icon name="trash alternate" />
        Delete order
      </Header>
      <Modal.Content>
        <p>
          Delete the order amount of <strong>{order.price}$</strong> that
          expires on{" "}
          <strong>{moment(order.expirationDate).format("LLL")}</strong>
        </p>
      </Modal.Content>
      <Modal.Actions style={{ textAlign: "center" }}>
        <button
          className="ui red inverted button"
          onClick={() => setOpen(false)}
        >
          <Icon name="remove" /> No
        </button>
        <button className="ui green inverted button" onClick={deleteOrder}>
          <Icon name="checkmark" /> Yes
        </button>
      </Modal.Actions>
    </Modal>
  );
};
