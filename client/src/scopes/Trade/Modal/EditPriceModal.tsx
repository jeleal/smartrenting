import React, { SetStateAction, Dispatch, useState, useEffect } from "react";
import { Button, Header, Icon, Input, Label, Modal } from "semantic-ui-react";
import { Order } from "../../../constants/types";
import Post from "../../../utils/Post";
import moment from "moment";
import { useAppContext } from "../../../contexts/AppContext";

export const EditPriceModal = ({
  open,
  order,
  setOpen,
  refetch,
}: {
  open: boolean;
  order: Order;
  setOpen: Dispatch<SetStateAction<boolean>>;
  refetch: Dispatch<SetStateAction<boolean>>;
}) => {
  const [price, setPrice] = useState<number>();
  const [error, setError] = useState<string>("");
  const { token, setToken } = useAppContext();

  const submitEditPrice = () => {
    if (price && price > 0) {
      Post({
        uri: "/order/edit",
        data: { price: price, id: order.id },
        token,
      })
        .then(() => {
          refetch((val) => !val);
          setError("");
          setOpen(false);
        })
        .catch((err) => {
          if (err.response.status === 401 && token) {
            localStorage.removeItem("token");
            setToken(null);
            setOpen(false);
          } else {
            refetch((val) => !val);
            setOpen(false);
          }
        });
    } else {
      setError("The price must be greater than 0 and a valid number");
    }
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { target } = e;
    if (target.value !== "") {
      setPrice(parseInt(target.value));
    } else {
      setPrice(null);
    }
  };
  useEffect(() => {
    setPrice(order.price);
    setError("");
  }, [order.price]);

  return (
    <Modal
      basic
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      size="small"
    >
      <Header icon>
        <Icon name="pencil alternate" />
        This order expires on {moment(order.expirationDate).format("LLL")}
      </Header>
      <Modal.Content style={{ textAlign: "center" }}>
        Edit the price:{" "}
        <>
          <Input
            className="input-transparent"
            error={error !== ""}
            style={{ backgroundColor: "transparent!important" }}
            size="mini"
            value={price || ""}
            onChange={handleChange}
            type="number"
          />
        </>
        <div>
          {error && (
            <Label pointing color="red">
              {error}
            </Label>
          )}
        </div>
      </Modal.Content>

      <Modal.Actions style={{ textAlign: "center" }}>
        <Button basic color="red" inverted onClick={() => setOpen(false)}>
          <Icon name="remove" /> Cancel
        </Button>
        <Button color="green" inverted onClick={submitEditPrice}>
          <Icon name="checkmark" /> Edit
        </Button>
      </Modal.Actions>
    </Modal>
  );
};
