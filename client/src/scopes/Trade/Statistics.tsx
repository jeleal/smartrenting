import React, { useEffect, useState } from "react";
import moment from "moment";
import Chart from "react-google-charts";
import { Grid, Icon, Popup } from "semantic-ui-react";

import { Order } from "../../constants/types";
import DatePicker from "react-datepicker";
import Post from "../../utils/Post";
import { useAppContext } from "../../contexts/AppContext";

import "./Trade.css";
import "react-datepicker/dist/react-datepicker.min.css";

export const Statistics = ({ orders }: { orders: Order[] }) => {
  const dataObject: Record<string, number> = {};
  const [startDate, setStartDate] = useState<Date>(new Date());
  const [orderExpire, setOrderExpire] = useState<Order[]>([]);
  const [totalExpirePrice, setTotalExpire] = useState<number>();
  const { token, setToken } = useAppContext();
  const dataArray = [];
  let averageOrders: number = 0;

  orders.forEach((order) => {
    if (dataObject[order.user.username]) {
      dataObject[order.user.username] += order.price;
    } else {
      dataObject[order.user.username] = order.price;
    }
  });

  for (const [key, value] of Object.entries(dataObject)) {
    dataArray.push([key, value, `${key} - ${value.toLocaleString()}$`]);
    averageOrders += value;
  }

  useEffect(() => {
    const date = moment(startDate).format("YYYY-MM-DD");
    Post({ uri: "/order/by-date", data: { date }, token })
      .then(({ data }: { data: Order[] }) => {
        const total = Object.values(data).reduce(
          (prev, { price }) => prev + price,
          0
        );
        setTotalExpire(total);
        setOrderExpire(data);
      })
      .catch((err) => {
        if (err.response.status === 401 && token) {
          localStorage.removeItem("token");
          setToken(null);
        }
      });
  }, [startDate]);

  return (
    <>
      <Grid columns={4}>
        <Grid.Column width={4} verticalAlign="middle">
          Average orders: {(averageOrders / orders.length).toFixed(2)}$
        </Grid.Column>
        <Grid.Column verticalAlign="middle">
          <div style={{ display: "inline-block" }}>
            <DatePicker
              selected={startDate}
              minDate={new Date()}
              dateFormat="MMMM d, yyyy"
              onChange={(date) => setStartDate(date)}
              placeholderText="Select a date"
            />
          </div>
          <div style={{ display: "inline-block", padding: "0 10px" }}>
            <Popup
              offset={[-12]}
              content="See the number of orders and the total amount that expire for that date"
              trigger={<Icon name="info circle" />}
            />
          </div>
        </Grid.Column>
        <Grid.Column verticalAlign="middle" width={6}>
          {orderExpire.length === 0 && `No order expires for this date`}
          {orderExpire.length === 1 &&
            `${
              orderExpire.length
            } order expire for an amount of ${totalExpirePrice.toLocaleString()}$`}
          {orderExpire.length > 1 &&
            `${
              orderExpire.length
            } orders expire for an amount of ${totalExpirePrice.toLocaleString()}$`}
        </Grid.Column>
      </Grid>
      <Chart
        width={"100%"}
        height={"350px"}
        chartType="PieChart"
        loader={<div>Loading Chart</div>}
        data={[
          ["User", "Total amount order", { role: "tooltip", type: "string" }],
          ...dataArray,
        ]}
        options={{
          backgroundColor: "transparent",
          legend: "none",
        }}
      />
    </>
  );
};
