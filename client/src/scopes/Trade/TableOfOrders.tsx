import React, { Dispatch, SetStateAction, useEffect, useState } from "react";
import { Icon, Table } from "semantic-ui-react";
import { Order } from "../../constants/types";
import { EditPriceModal } from "./Modal/EditPriceModal";
import moment from "moment";
import { DeleteOrderModal } from "./Modal/DeleteOrderModal";
moment.locale(navigator.language.split("-")[0]);

export default function TableOfOrders({
  orders,
  userId,
  refetch,
}: {
  orders: Order[];
  userId: number;
  refetch: Dispatch<SetStateAction<boolean>>;
}) {
  const [openEditModal, setOpenEditModal] = useState<boolean>(false);
  const [openDeleteOrderModal, setOpenDeleteOrderModal] =
    useState<boolean>(false);
  openDeleteOrderModal;
  const [selectedOrder, setSelectedOrder] = useState<Order>();

  const generateRow = () => {
    const currentDate = new Date();

    return orders.map((order: Order, index) => {
      const { user, price, expirationDate } = order;

      return (
        <Table.Row key={index}>
          <Table.Cell>{user.username}</Table.Cell>
          <Table.Cell>{price.toLocaleString()}$</Table.Cell>
          <Table.Cell width={5}>
            {moment(expirationDate).format("LLL")}
          </Table.Cell>
          <Table.Cell textAlign="center" width={3}>
            {user.id === userId && (
              <>
                <Icon
                  onClick={() => {
                    setOpenEditModal(true);
                    setSelectedOrder(order);
                  }}
                  style={{ cursor: "pointer", margin: "0 10px" }}
                  name="pencil alternate"
                  size="large"
                />
                <Icon
                  color="red"
                  onClick={() => {
                    setOpenDeleteOrderModal(true);
                    setSelectedOrder(order);
                  }}
                  style={{
                    cursor: "pointer",
                    margin: "0 10px",
                  }}
                  name="trash alternate"
                  size="large"
                />
              </>
            )}
          </Table.Cell>
        </Table.Row>
      );
    });
  };

  return (
    <div style={{ height: "300px", overflowY: "scroll", margin: "15px 0" }}>
      {selectedOrder && (
        <>
          <EditPriceModal
            setOpen={setOpenEditModal}
            refetch={refetch}
            open={openEditModal}
            order={selectedOrder}
          />
          <DeleteOrderModal
            setOpen={setOpenDeleteOrderModal}
            refetch={refetch}
            open={openDeleteOrderModal}
            order={selectedOrder}
          />
        </>
      )}
      <Table className="TabStyle" celled padded>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Username</Table.HeaderCell>
            <Table.HeaderCell>Price</Table.HeaderCell>
            <Table.HeaderCell>Expiration</Table.HeaderCell>
            <Table.HeaderCell>Action</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>{generateRow()}</Table.Body>
      </Table>
    </div>
  );
}
