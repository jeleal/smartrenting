import React from "react";
import { Link, useHistory } from "react-router-dom";
import { Grid, Icon } from "semantic-ui-react";
import { useAppContext } from "../../contexts/AppContext";
import "./Navbar.css";

const logoStyle = {
  width: "100%",
  height: window.innerWidth > 760 ? "100px" : "50px",
  backgroundImage: `url(https://cdn.worldvectorlogo.com/logos/gamestop.svg)`,
  backgroundSize: "contain",
  backgroundRepeat: "no-repeat",
  backgroundPosition: "center",
};

export default function Navbar() {
  const { token, setToken } = useAppContext();
  const history = useHistory();

  const logout = () => {
    setToken(null);
    localStorage.removeItem("token");
    history.replace("/login");
  };
  return (
    <div className="Navbar">
      <Grid columns={2}>
        <Grid.Column>
          <Link to="/trade">
            <img
              src="https://cdn.worldvectorlogo.com/logos/gamestop.svg"
              alt="Official logo"
              height="50px"
            />
          </Link>
        </Grid.Column>
        <Grid.Column>
          <div onClick={logout} style={{ cursor: "pointer" }}>
            <Icon name="log out" size="big" />
          </div>
        </Grid.Column>
      </Grid>
    </div>
  );
}
