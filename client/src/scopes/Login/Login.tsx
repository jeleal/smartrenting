import React, { useState } from "react";
import axios from "axios";
import { useHistory } from "react-router";
import { Form, Input, Message, Grid, Header } from "semantic-ui-react";
import { useAppContext } from "../../contexts/AppContext";

const logoStyle = {
  width: "100%",
  height: window.innerWidth > 760 ? "100px" : "50px",
  backgroundImage: `url(https://cdn.worldvectorlogo.com/logos/gamestop.svg)`,
  backgroundSize: "contain",
  backgroundRepeat: "no-repeat",
  backgroundPosition: "center",
};

export default function Login() {
  const history = useHistory();
  const { token, setToken } = useAppContext();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  const submitLogin = () => {
    if (username.trim().length > 2 && password.length > 4) {
      axios
        .post(`${process.env.REACT_APP_API_URL}/login`, {
          username,
          password,
        })
        .then(({ data }) => {
          setError("");
          setToken(data);
          localStorage.setItem("token", data);
          history.push(`/trade`);
        })
        .catch((error) => {
          if (typeof error.response.data.message !== "string") {
            setError(error.response.data.message[0]);
          } else {
            setError(error.response.data.message);
          }
        });
    } else {
      if (username.trim().length < 3) {
        setError("Username must be longer than or equal to 3 characters");
      } else {
        setError("Password must be longer than or equal to 5 characters");
      }
    }
  };

  return (
    <Grid textAlign="center" style={{ height: "100vh" }} verticalAlign="middle">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as="h2" color="teal" textAlign="center">
          <div style={logoStyle} />
        </Header>
        <Form error onSubmit={submitLogin}>
          <Form.Field
            icon="user"
            iconPosition="left"
            placeholder="username"
            control={Input}
            onChange={({ target }) => setUsername(target.value)}
            error={error !== ""}
          />
          <Form.Field
            icon="lock"
            iconPosition="left"
            placeholder="password"
            type="password"
            control={Input}
            autoComplete="on"
            onChange={({ target }) => setPassword(target.value)}
            error={error !== ""}
          />
          <button className="ui button" type="submit">
            Submit
          </button>
          {error && <Message error={error !== ""} content={`${error}`} />}
        </Form>
      </Grid.Column>
    </Grid>
  );
}
