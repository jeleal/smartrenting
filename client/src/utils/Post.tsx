import React from "react";
import axios from "axios";

export default function Post({
  uri,
  data,
  token,
}: {
  uri: string;
  data: Record<string, unknown>;
  token: string;
}): Promise<{ data: any }> {
  // Le context ne fonctionne pas dans une fonction, checker pourquoi
  // const { token } = useAppContext();

  return axios.post(
    `${process.env.REACT_APP_API_URL}${uri}`,
    { ...data },
    {
      headers: {
        authorization: `Bearer ${token}`,
      },
    }
  );
}
