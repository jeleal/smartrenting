import React, { useEffect, useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Login from "./scopes/Login/Login";
import "./App.css";
import { AppProvider, useAppContext } from "./contexts/AppContext";
import TokenHandler from "./scopes/TokenHandler/TokenHandler";
import Trade from "./scopes/Trade/Trade";
import Navbar from "./scopes/Navbar/Navbar";
import Post from "./utils/Post";

function App() {
  const { token, setToken } = useAppContext();
  const [userId, setUserId] = useState<number>();

  useEffect(() => {
    Post({ uri: "/whoami", data: null, token })
      .then(({ data }) => {
        setUserId(data.id);
      })
      .catch((err) => {
        if (err.response.status === 401 && token) {
          localStorage.removeItem("token");
          setToken(null);
        }
      });
  }, [token]);

  return (
    <Router>
      <div className="App">
        {token && <Navbar />}
        <Switch>
          <Route path="/login" component={Login} />
          <Route path="/trade">
            <Trade userId={userId} />
          </Route>
        </Switch>
      </div>
      <Route path="*" component={TokenHandler} />
    </Router>
  );
}
const WrappedApp = () => (
  <AppProvider>
    <App />
  </AppProvider>
);

export default WrappedApp;
