export interface User {
  id: number;
  username: string;
  orders: Order[];
}

export interface Order {
  id: number;
  price: number;
  expirationDate: string;
  user: {
    id: number;
    username: string;
  };
}
