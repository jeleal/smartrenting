import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { OrderService } from 'src/order/order.service';

@Injectable()
export class CronService {
  constructor(private readonly orderService: OrderService) {}

  @Cron('* * * * *')
  async delete() {
    await this.orderService.deleteExpiredOrders();
  }
}
