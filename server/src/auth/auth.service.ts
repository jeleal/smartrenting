import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { User } from 'src/user/user.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(private readonly jwtService: JwtService) {}

  generateJWT(user: User): Promise<string> {
    const data = {
      id: user.id,
      username: user.username,
    };

    return this.jwtService.signAsync({ user: data });
  }

  hashPassword(password: string): string {
    return bcrypt.hash(password, 12);
  }

  comparePasswords(password: string, passwordHash: string): boolean {
    return bcrypt.compare(password, passwordHash);
  }
}
