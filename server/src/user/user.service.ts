import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { Repository } from 'typeorm';
import { User } from './user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    private readonly authService: AuthService,
  ) {}

  whoami(id: number): Promise<User> {
    return this.userRepository.findOne({
      where: { id },
      relations: ['orders'],
    });
  }

  async create(username: string, password: string): Promise<string> {
    const newUser = new User();

    newUser.username = username;
    newUser.password = await this.authService.hashPassword(password);
    const userSaved = await this.userRepository.save(newUser);

    return this.authService.generateJWT(userSaved);
  }

  async login(username: string, password: string): Promise<string> {
    const user = await this.userRepository.findOne({
      where: { username: username },
      select: ['password', 'username', 'id'],
    });

    if (!user) {
      return this.create(username, password);
    }

    const match = await this.authService.comparePasswords(
      password,
      user.password,
    );

    if (match === false) {
      throw new HttpException('wrong credentials', HttpStatus.FORBIDDEN);
    }

    return this.authService.generateJWT(user);
  }
}
