import { IsNotEmpty, MaxLength, MinLength } from 'class-validator';

export class loginDto {
  @IsNotEmpty()
  @MinLength(3)
  @MaxLength(30)
  username: string;

  @IsNotEmpty()
  @MinLength(5)
  @MaxLength(40)
  password: string;
}
