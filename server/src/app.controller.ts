import { Body, Controller, Post, Request, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from './auth/guards/jwt-guard';
import { Order } from './order/order.entity';
import { OrderService } from './order/order.service';
import { loginDto } from './user/user.dto';
import { User } from './user/user.entity';
import { UserService } from './user/user.service';

@Controller()
export class AppController {
  constructor(
    private readonly userService: UserService,
    private readonly orderService: OrderService,
  ) {}

  @Post('/login')
  login(@Body() body: loginDto): Promise<string> {
    return this.userService.login(body.username, body.password);
  }

  @Post('whoami')
  @UseGuards(JwtAuthGuard)
  whoami(@Request() { user }: { user: User }): User {
    return user;
  }

  @Post('order')
  @UseGuards(JwtAuthGuard)
  order(): Promise<Order[]> {
    return this.orderService.findAllOrders();
  }
}
