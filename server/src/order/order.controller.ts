import { Body, Controller, Post, Request, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/guards/jwt-guard';
import { User } from 'src/user/user.entity';
import { CreateOrderDto, UpdateOrderDto } from './order.dto';
import { Order } from './order.entity';
import { OrderService } from './order.service';

@Controller('order')
@UseGuards(JwtAuthGuard)
export class OrderController {
  constructor(private readonly orderService: OrderService) {}
  @Post('create')
  create(
    @Body() { price }: CreateOrderDto,
    @Request() { user }: { user: User },
  ): Promise<Order> {
    return this.orderService.createOrder(price, user);
  }

  @Post('edit')
  edit(
    @Body() { price, id }: UpdateOrderDto,
    @Request() { user }: { user: User },
  ): Promise<Order> {
    return this.orderService.editOrder(id, price, user.id);
  }

  @Post('delete')
  delete(
    @Body() { id }: UpdateOrderDto,
    @Request() { user }: { user: User },
  ): Promise<string> {
    return this.orderService.deleteOrder(id, user.id);
  }

  @Post('/by-date')
  byDate(@Body() { date }: { date: string }): Promise<Order[]> {
    return this.orderService.findOrdersByDate(date);
  }
}
