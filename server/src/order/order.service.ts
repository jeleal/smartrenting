import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/user/user.entity';
import { Repository, Equal } from 'typeorm';
import { Order } from './order.entity';

@Injectable()
export class OrderService {
  constructor(
    @InjectRepository(Order)
    private readonly orderRepository: Repository<Order>,
  ) {}

  createOrder(price: number, user: User): Promise<Order> {
    const newOrder = new Order();
    const date = new Date();

    date.setDate(date.getDate() + 14);
    newOrder.price = price;
    newOrder.expirationDate = date;
    newOrder.user = user;

    return this.orderRepository.save(newOrder);
  }

  async editOrder(id: number, price: number, userId: number): Promise<Order> {
    const order = await this.finOneOrder(id);

    if (!order || order.user.id !== userId) {
      throw new Error('Order not found');
    }

    order.price = price;
    return this.orderRepository.save(order);
  }

  finOneOrder(id: number): Promise<Order> {
    return this.orderRepository.findOne({ where: { id }, relations: ['user'] });
  }

  findAllOrders(): Promise<Order[]> {
    return this.orderRepository.find({
      relations: ['user'],
    });
  }

  async deleteOrder(id: number, userId: number): Promise<string> {
    const order = await this.finOneOrder(id);

    if (!order || order.user.id !== userId) {
      throw new Error('Order not found');
    }

    return this.orderRepository.delete(id).then(() => 'success');
  }

  deleteExpiredOrders(): void {
    const currentDate = new Date();

    this.orderRepository
      .createQueryBuilder()
      .delete()
      .where('expirationDate <= :currentDate', { currentDate })
      .execute();
  }

  findOrdersByDate(date: string): Promise<Order[]> {
    return this.orderRepository
      .createQueryBuilder()
      .where(`DATE_TRUNC('day', "expirationDate") = :date`, {
        date,
      })
      .getMany();
  }
}
