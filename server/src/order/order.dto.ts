export class CreateOrderDto {
  price: number;
}

export class UpdateOrderDto {
  id: number;
  price: number;
}
